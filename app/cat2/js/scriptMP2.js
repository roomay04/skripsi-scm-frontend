let app = new function() {

  let deletingID;
  let updatingID;
  let producingID;
  let qcID;

  let ascendingName      = false;
  let ascendingPrice     = true;
  let lastSelectedFilter = 'name';

  let getMaterialCode  = document.getElementById('add-material-code');
  let getQuantity = document.getElementById('add-quantity');
  let getWorkCentreCode = document.getElementById('add-wc-code');
  let getDate = document.getElementById('add-date');

  let editQuantity = document.getElementById('edit-quantity');

  let addModule    = document.getElementById('add-module');
  let editModule   = document.getElementById('edit-module');
  let deleteModule = document.getElementById('delete-module');
  let produceModule = document.getElementById('produce-module');
  let infoModule = document.getElementById('info-module');
  let qcModule = document.getElementById('qc-module');
  let inactiveMask = document.getElementById('inactive-mask');

  let rejectMaterialQuantity = document.getElementById('reject-quantity');

  let addErrorMaterialField  = document.getElementById('add-error-information-material');
  let addErrorQuantityField = document.getElementById('add-error-information-quantity');
  let addErrorWorkCentreField = document.getElementById('add-error-information-wc');
  let addErrorDateField = document.getElementById('add-error-information-date');

  let editErrorQuantityField = document.getElementById('edit-error-information-quantity');

  let rejectErrorField = document.getElementById('reject-error-information');


  this.table    = document.getElementById('products');
  var host = 'https://standard.kingwijati.com/'

  this.FetchAll = function() {
    fetch(host+'api/materialProcess/list.abs')
    .then(response => response.json())
    .then(function(data) {
      data.data.sort(function(a, b) {
        return (a.id - b.id);
      });
      console.log(data);

      let resultCreated = ''
      let resultQC = ''
      let resultDone = ''
      data.data.forEach((product) => {
        if (product.statusProcess === 'created') {
          resultCreated += '<tr>';
          resultCreated += '<td class="app__table-name-value">' + product.id +
                    '</td>';
          resultCreated += '<td class="app__table-name-value">' + product.code +
                    '</td>';
          resultCreated += '<td>' + product.quantity
                  + '</td>';
          resultCreated += '<td>' + product.workCentreCode
                  + '</td>';
          resultCreated += '<td>' + product.datestamp
                  + '</td>';
          resultCreated += '<td>' + product.statusProcess
                  + '</td>';
          resultCreated += '<td><div class="app__table-actions-block">' +
                  '<button onclick="app.Produce(' + product.id + ')">Produce</button>' +
                  '<button onclick="app.Edit(' + product.id + ')">Edit</button></div></td>';
          resultCreated += '</tr>';
        } else if (product.statusProcess === 'processed'){
          resultQC += '<tr>';
          resultQC += '<td class="app__table-name-value">' + product.id +
                    '</td>';
          resultQC += '<td class="app__table-name-value">' + product.code +
                    '</td>';
          resultQC += '<td>' + product.quantity
                  + '</td>';
          resultQC += '<td>' + product.workCentreCode
                  + '</td>';
          resultQC += '<td>' + product.datestamp
                  + '</td>';
          resultQC += '<td>' + product.statusProcess
                  + '</td>';
          resultQC += '<td><div class="app__table-actions-block">' +
                  '<button onclick="app.QC(' + product.id + ')">QC</button></div></td>';
          resultQC += '</tr>';
        } else if (product.statusProcess === 'finished'){
          resultDone += '<tr>';
          resultDone += '<td class="app__table-name-value">' + product.id +
                    '</td>';
          resultDone += '<td class="app__table-name-value">' + product.code +
                    '</td>';
          resultDone += '<td>' + product.quantity
                  + '</td>';
          resultDone += '<td>' + product.workCentreCode
                  + '</td>';
          resultDone += '<td>' + product.datestamp
                  + '</td>';
          resultDone += '<td>' + product.statusProcess
                  + '</td>';
          resultDone += '</tr>';
        }
      });
      document.getElementById('createdTable').innerHTML = resultCreated;
      document.getElementById('qcTable').innerHTML = resultQC;
      document.getElementById('doneTable').innerHTML = resultDone;       
    });
  };

  this.Add = function() {
    
    let materialCode = getMaterialCode.value;
    let quantity = getQuantity.value;
    let workCentreCode = getWorkCentreCode.value;
    let date = getDate.value;

    if (this.Check('get')) {
        addModule.style.display = 'none';
        inactiveMask.style.display = 'none';

        fetch(host+'api/materialProcess/save.abs?code='+materialCode+'&quantity='+quantity+'&workcentrecode='+workCentreCode+'&datestamp='+date)
        .then(response => response.json())
        .then(function(data) {
          console.log(data);
        })
        .then(() => {
          this.FetchAll();
          getMaterialCode.value  = '';
          getQuantity.value = '';
          getWorkCentreCode.value = '';
          getDate.value = '';
        });
      }
  };

  this.AddNew = function() {
    addModule.style.display = 'flex';
    inactiveMask.style.display = 'block';

    getQuantity.value = '';
    getDate.value = '';

    addErrorMaterialField.innerHTML = '';
    addErrorQuantityField.innerHTML = '';
    addErrorWorkCentreField.innerHTML = '';
    addErrorDateField.innerHTML = '';

    Promise.all([
      fetch(host+'api/product/list.abs?'),
      fetch(host+'api/workcentre/list.abs?')
    ])
    .then(result => Promise.all(result.map(v => v.json()))
    .then(function(data) {
      console.log(data[0]);
      console.log(data[1]);

      let materialData = '';
      let workCentreData = '';

      data[0].data.forEach((material) => {
        if (material.materialType === 'FG') {
          materialData += '<option value="'+material.code+'">'+material.code+'</option>';
        } 
      });
      document.getElementById('add-material-code').innerHTML = materialData;

      data[1].data.forEach((workCentre) => {
        workCentreData += '<option value="'+workCentre.code+'">'+workCentre.code+'</option>';
      });
      document.getElementById('add-wc-code').innerHTML = workCentreData;

      getMaterialCode.style.borderColor  = '#0C2427';
      getQuantity.style.borderColor = '#0C2427';
      getWorkCentreCode.style.borderColor = '#0C2427';
      getDate.style.borderColor = '#0C2427';
    }));
  };

  this.closeAddForm = function() {
    addModule.style.display = 'none';
    inactiveMask.style.display = 'none';
  };

  this.Delete = function(item) {
    deletingID = item;
    deleteModule.style.display = 'flex';
    inactiveMask.style.display = 'block';
  };

  this.closeDeleteForm = function() {
    deleteModule.style.display = 'none';
    inactiveMask.style.display = 'none';
  };

  this.applyDeleteForm = function() {
    deleteModule.style.display = 'none';
    inactiveMask.style.display = 'none';
    fetch(host+'api/billOfMaterial/delete.abs?id='+deletingID)
    .then(response => response.json())
    .then(function(data) {
      console.log(data);
    })
    .then(() => {
      this.FetchAll();
    });
  };

  this.Produce = function(item) {
    producingID = item;
    produceModule.style.display = 'flex';
    inactiveMask.style.display = 'block';
  };

  this.closeProduceForm = function() {
    produceModule.style.display = 'none';
    inactiveMask.style.display = 'none';
  };

  this.applyProduceForm = function() {
    produceModule.style.display = 'none';
    inactiveMask.style.display = 'none';
    fetch(host+'api/materialProcess/production.abs?id='+producingID)
    .then(response => response.json())
    .then(function(data) {
      console.log(data);
      infoModule.style.display = 'flex';
      inactiveMask.style.display = 'block';
      document.getElementById('info-text-module').innerHTML = data.data.description;
    });
  };

  this.closeInfoForm = function() {
    infoModule.style.display = 'none';
    inactiveMask.style.display = 'none';
    this.FetchAll();
  }

  this.Edit = function(item) {
    editModule.style.display = 'flex';
    inactiveMask.style.display = 'block';

    updatingID = item;

    editQuantity.style.borderColor = '#0C2427';

    fetch(host+'api/materialProcess/detail.abs?id='+updatingID)
    .then(response => response.json())
    .then(function(data) {
      console.log(data.data);
      editQuantity.value = data.data.quantity;

      editErrorQuantityField.innerHTML = '';
    });
  };

  this.closeEditForm = function() {
   editModule.style.display = 'none';
   inactiveMask.style.display = 'none';
  }

  this.applyEditForm = function() {
    if (this.Check('edit')) {
      fetch(host+'api/materialProcess/detail.abs?id='+updatingID)
        .then(response => response.json())
        .then(function(data) {
          console.log(data);
          let code = data.data.code;
          let workCentreCode = data.data.workCentreCode;
          let statusProcess = data.data.statusProcess;
          let quantity = editQuantity.value;
          let datestamp = data.data.datestamp;

          fetch(host+'api/materialProcess/update.abs?id='+updatingID+'&code='+code+'&workcentrecode='+workCentreCode+'&status='+statusProcess+'&quantity='+quantity+'&datestamp='+datestamp)
          .then(response => response.json())
          .then(function(data) {
            console.log(data);
            window.location.reload(true);
          });
        });
     }
   };

  this.QC = function(item) {
    qcID = item;
    qcModule.style.display = 'flex';
    inactiveMask.style.display = 'block';

    rejectErrorField.innerHTML = '';
  };

  this.applyQCForm = function() {
    fetch(host+'api/materialProcess/detail.abs?id='+qcID)
      .then(response => response.json())
      .then(function(data) {
        let checker = true;
        console.log(data);
        if (rejectMaterialQuantity.value > data.data.quantity) {
          rejectMaterialQuantity.style.borderColor = '#F2223C';
          rejectErrorField.innerHTML = 'The rejected amount is higher than the processed amount';
          checker = false;
        }
        if (rejectMaterialQuantity.value === '') {
          rejectMaterialQuantity.style.borderColor = '#F2223C';
          rejectErrorField.innerHTML = 'Please, enter the value.';
          checker = false;
        }
        return checker;
      }).then(function(checker) {
        console.log(checker);
        if(checker) {
          let quantity = rejectMaterialQuantity.value;

          fetch(host+'api/materialProcess/qc.abs?id='+qcID+'&reject='+quantity)
          .then(response => response.json())
          .then(function(data) {
            console.log(data);
          }).then(() => {
            qcModule.style.display = 'none';
            inactiveMask.style.display = 'none';
            window.location.reload(true);
          });            
        }
      });
  }

  this.closeQCForm = function() {
    qcModule.style.display = 'none';
    inactiveMask.style.display = 'none';
   }

//   this.Sort = function(param) {
//     if (param === 'name') {
//       if (ascendingName) {
//         this.products.sort(compareDescendingName);
//         ascendingName = false;
//         lastSelectedFilter = 'name';
//         document.getElementById("table-sort-triangle-name").innerHTML="&#9660;"
//       } else {
//         this.products.sort(compareAscendingName);
//         ascendingName = true;
//         lastSelectedFilter = 'name';
//         document.getElementById("table-sort-triangle-name").innerHTML="&#9650;"
//       }
//     };

//     if (param === 'price') {
//       if (ascendingPrice) {
//         this.products.sort(compareDescendingPrice);
//         ascendingPrice = false;
//         lastSelectedFilter = 'price';
//         document.getElementById("table-sort-triangle-price").innerHTML="&#9660;"
//       } else {
//         this.products.sort(compareAscendingPrice);
//         ascendingPrice = true;
//         lastSelectedFilter = 'price';
//         document.getElementById("table-sort-triangle-price").innerHTML="&#9650;"
//       }
//     }

//     this.FetchAll();

//     function compareAscendingPrice(x, y) {
//       return x.price - y.price;
//     }
//     function compareDescendingPrice(x, y) {
//       return y.price - x.price;
//     }

//     function compareAscendingName(x, y) {
//       if (x.name > y.name) return 1;
//       if (x.name < y.name) return -1;
//     }
//     function compareDescendingName(x, y) {
//       if (x.name > y.name) return -1;
//       if (x.name < y.name) return 1;
//     }
//   };

//   // сортировка элементов таблицы по параметру name при загрузке страницы
//   this.Sort('name');

//   this.Search = function() {
//     let searchValue = document.getElementById('search-form').value.toLowerCase().trim();
//     let searchArray = this.products;
//     let searchCompleted = [];

//     for (i = 0; i < searchArray.length; i++) {
//       let currentStr = searchArray[i].name.slice(0, searchValue.length).toLowerCase();
//       if (searchValue === currentStr) {
//         searchCompleted.push(searchArray[i]);
//       }
//     }

//     this.products = searchCompleted;
//     this.FetchAll();
//     document.getElementById('search-form').value = '';
//   };

//   /*
//    * Проверяет корректность заполнения формы
//    *
//    * @param {get} - проверка формы добавления нового элемента
//    * @param {edit} - проверка формы редактирования элемента
//    * return {true/false} - корректно/некорректно заполнена форма
//    */
  this.Check = function(param) {
    let checker = true;

    if (param === 'get') {
      if (getMaterialCode.value === '') {
        getMaterialCode.style.borderColor = '#F2223C';
        addErrorMaterialField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      if (getQuantity.value === '') {
        getQuantity.style.borderColor = '#F2223C';
        addErrorQuantityField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      if (getWorkCentreCode.value === '') {
        getWorkCentreCode.style.borderColor = '#F2223C';
        addErrorWorkCentreField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      if (getDate.value === '') {
        getDate.style.borderColor = '#F2223C';
        addErrorDateField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      return checker;
    }

    if (param === 'edit') {
      if (editQuantity.value === '') {
        editQuantity.style.borderColor = '#F2223C';
        editErrorQuantityField.innerHTML = 'Please, enter the value.';
        checker = false;
      }
      return checker;
    }
  };

//   // подсвечивание некорректных значений после потери фокуса

//   getName.onblur = function() {
//     if (getName.value.trim() === '') {
//       getName.style.borderColor = '#F2223C';
//       addErrorNameField.innerHTML = 'Please, enter the value.';
//     }
//     if (getName.value.trim().length > 15) {
//       getName.style.borderColor = '#F2223C';
//       addErrorNameField.innerHTML = 'Sorry, but the field can\'t ' +
//                                     ' exceed 15 characters.';
//     }
//   }
//   getName.onfocus = function() {
//       getName.style.borderColor = '#0C2427';
//       addErrorNameField.innerHTML = '';
//   };

//   getCount.onblur = function() {
//     if (getCount.value.trim() === '') {
//       getCount.style.borderColor = '#F2223C';
//       addErrorCountField.innerHTML = 'Please, enter the value.';
//     }
//   }
//   getCount.onfocus = function() {
//       getCount.style.borderColor = '#0C2427';
//       addErrorCountField.innerHTML = '';
//   };

//   getPrice.onblur = function() {
//     if (getPrice.value.trim() === '') {
//       getPrice.style.borderColor = '#F2223C';
//       addErrorPriceField.innerHTML = 'Please, enter the value.';
//     }
//   }
//   getPrice.onfocus = function() {
//       getPrice.style.borderColor = '#0C2427';
//       addErrorPriceField.innerHTML = '';
//   };

//   editName.onblur = function() {
//     if (editName.value.trim() === '') {
//       editName.style.borderColor = '#F2223C';
//       editErrorNameField.innerHTML = 'Please, enter the value.';
//     }
//     if (editName.value.trim().length > 15) {
//       getName.style.borderColor = '#F2223C';
//       editErrorNameField.innerHTML = 'Sorry, but the field can\'t ' +
//                                      ' exceed 15 characters.';
//     }
//   }
//   editName.onfocus = function() {
//       editName.style.borderColor = '#0C2427';
//       editErrorNameField.innerHTML = '';
//   };

//   editCount.onblur = function() {
//     if (editCount.value.trim() === '') {
//       editCount.style.borderColor = '#F2223C';
//       editErrorCountField.innerHTML = 'Please, enter the value.';
//     }
//   }
//   editCount.onfocus = function() {
//       editCount.style.borderColor = '#0C2427';
//       editErrorCountField.innerHTML = '';
//   };

//   editPrice.onblur = function() {
//     if (editPrice.value.trim() === '') {
//       editPrice.style.borderColor = '#F2223C';
//       editErrorPriceField.innerHTML = 'Please, enter the value.';
//     }
//   }
//   editPrice.onfocus = function() {
//       editPrice.style.borderColor = '#0C2427';
//       editErrorPriceField.innerHTML = '';
//   };

};
app.FetchAll();

